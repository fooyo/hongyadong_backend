ActiveAdmin.register_page "Dashboard" do
  content title: proc { I18n.t("active_admin.dashboard") } do

    panel "8:50 数据" do
    table_for VisitorOffset.first do |v|
      column("入园") {|p| p.enter_offset}
      column("出园") {|p| p.leave_offset}
      column("今日接待") {|p| p.today_visitor_offset}
      column("更新时间") {|p| p.updated_at }
    end
  end

  panel "网页浏览量" do
    table_for Pageview.first do |p|
      column("网页浏览量") {|p| p.count}
    end
  end
end
end