ActiveAdmin.register SiteNotification do


  permit_params :content

  index do
    selectable_column
    id_column
    column :content
    column :created_at
    column :updated_at
    column :readable_type
    actions
  end

  filter :content

  show do
    attributes_table do
      row :id
      row :content
      row :created_at
      row :updated_at
      row :readable_type
    end
  end

  form do |f|
    f.inputs do
      f.input :content
    end
    f.actions
  end

end
