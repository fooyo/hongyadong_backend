ActiveAdmin.register SmsGroup do

  permit_params staff_ids: []

  actions :index, :show, :update, :edit


  index do
    selectable_column
    id_column
    column :name
    column :lower_limit
    column :upper_limit
    list_column :staff_names
    actions
  end

  filter :name

  show do
    attributes_table do
      row :id
      row :name
      row :created_at
      row :updated_at
      list_row :staff_names
    end
  end

  form do |f|
    f.inputs do
      f.input :name, disabled: true
      f.input :staff_ids, as: :select, multiple: true, collection: Staff.all.map {|a| [a.desc, a.id]}
      f.input :lower_limit, disabled: true
      f.input :upper_limit, disabled: true
    end
    f.actions
  end

end
