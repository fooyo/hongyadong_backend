ActiveAdmin.register Staff do

  permit_params :name, :mobile_number

  index do
    selectable_column
    id_column
    column :name
    column :mobile_number
    list_column :group_names
    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :mobile_number
      row :created_at
      row :updated_at
      list_row :group_names
    end
  end


  filter :name
  filter :mobile_number

  form do |f|
    f.inputs do
      f.input :name
      f.input :mobile_number
    end
    f.actions
  end

end
