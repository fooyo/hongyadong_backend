ActiveAdmin.register VisitorOffset do
  permit_params :enter_offset, :leave_offset, :today_visitor_offset, :enter_at_eleven, :leave_at_eleven,
                :today_at_eleven, :use_tensorflow

end
