class ApplicationController < ActionController::Base

  def render_custom_error(errors, status = :unprocessable_entity)
    errors = [errors] unless errors.is_a?(Array)
    render json: {errors: errors}, status: status
  end

end
