class V1::DoorRecordsController < ApiController

  def site_notifications
    @site_notifications = SiteNotification.unscoped.door_record.order(created_at: :desc)
  end


  def report
    d = DoorRecord.where(created_at: Time.now.beginning_of_day..Time.now.end_of_day).first_or_initialize do |i|
      i.enter_count = 0
      i.leave_count = 0
      i.reported_at = Time.now
    end

    d.reported_at = report_params[:reported_at]
    d.enter_count = report_params[:enter_count].to_i
    d.leave_count = report_params[:leave_count].to_i
    if d.save
      head :no_content
    else
      render_custom_error d.errors.to_a
    end
  end

  private
  def report_params
    params.require(:door_record).permit(:leave_count, :enter_count, :reported_at)
  end
end
