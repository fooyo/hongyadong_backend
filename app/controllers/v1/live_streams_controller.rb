class V1::LiveStreamsController < ApiController
  def index
    if params[:q].present?
      q = URI.decode(params[:q])
      @live_stream = LiveStream.where("name like ?", "%#{q}%")
    else
      @live_streams = LiveStream.all
    end
  end
end
