class V1::PageviewsController < ApiController
  def create
    p = Pageview.first_or_initialize
    p.count += 1
    p.save
  end
end
