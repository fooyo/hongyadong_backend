class V1::WeatherController < ApiController

  def current
    # render json:
    #            {"current": {
    #                "weather": "rain",
    #                "temperature": 21,
    #                "time": Time.now
    #            }
    #            }
    weather = WeatherService.new.current_weather
    weather[:time] = Time.now
    render json: weather
  end

  def current_and_forecasts
    # now = Time.now
    # start = now.beginning_of_hour
    # json  = {
    #     "current": {
    #         "weather": "rain",
    #         "temperature": 21,
    #         "time": start
    #     },
    #     "forecasts": [
    #         {
    #             "time": start + 1.hours,
    #             "weather": "rain",
    #             "temperature": 21
    #         },
    #         {
    #             "time": start + 2.hours,
    #             "weather": "rain",
    #             "temperature": 20
    #         },
    #         {
    #             "time": start + 3.hours,
    #             "weather": "rain",
    #             "temperature": 21
    #         },
    #         {
    #             "time": start + 4.hours,
    #             "weather": "rain",
    #             "temperature": 21
    #         }
    #     ]
    # }
    #
    # render json: json
    s = WeatherService.new
    current = s.current_weather
    current[:time] = Time.now
    i = 1
    forecasts = [current, current, current, current].map do |a|
      r = {
          time: a[:time] + i.hours,
          temperature: a[:temperature],
          weather: a[:weather]
      }
      i += 1
      r
    end
    # forecasts = s.hourly_forecasts
    render json: {
        current: current,
        forecasts: forecasts
    }
  end

end
