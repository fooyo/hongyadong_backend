class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  MAX_VISITOR_VOLUME = 8750
end
