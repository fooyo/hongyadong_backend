# == Schema Information
#
# Table name: door_records
#
#  id          :bigint(8)        not null, primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  reported_at :datetime
#  enter_count :integer
#  leave_count :integer
#

class DoorRecord < ApplicationRecord

end
