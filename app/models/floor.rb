# == Schema Information
#
# Table name: floors
#
#  id                :bigint(8)        not null, primary key
#  name              :string(255)
#  sub_scenic_id     :integer
#  queue_count       :integer          default(0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  realtime_visitors :integer
#  max_visitors      :integer
#

class Floor < ApplicationRecord
  has_many :door_records

  has_many :wifi_points
  has_many :live_streams

  validates_uniqueness_of :sub_scenic_id

  def visitor_volume_status
    max = max_visitors || 1330
    ratio = (realtime_visitors || 0) / max.to_f
    if ratio >= 1
      "客流拥挤"
    elsif ratio >= 0.85
      "客流缓行"
    else
      "客流畅通"
    end
  end

  def floor
    sub_scenic_id
  end

  def floor_image

  end
end
