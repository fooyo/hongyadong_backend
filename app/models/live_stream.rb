# == Schema Information
#
# Table name: live_streams
#
#  id           :bigint(8)        not null, primary key
#  hls_url      :string(255)
#  name         :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  floor_id     :integer
#  point_id     :integer
#  channel_type :integer
#

class LiveStream < ApplicationRecord
  belongs_to :floor, touch: true

  default_scope {where.not(hls_url: nil)}

end
