# == Schema Information
#
# Table name: site_notifications
#
#  id                :bigint(8)        not null, primary key
#  content           :text(65535)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  notification_type :integer          default("custom")
#  deleted           :boolean          default(FALSE)
#  enter_count       :integer
#  leave_count       :integer
#

class SiteNotification < ApplicationRecord
  enum notification_type: %w(custom eight_k eight_seven_k door_record)

  validate :unique_non_custom

  default_scope {where(deleted: false).where.not(notification_type: 'door_record').order(updated_at: :desc)}

  def readable_type
    case notification_type
    when 'custom'
      "人工添加"
    when 'eight_k'
      "8000客流量预警 - 系统生成"
    when 'eight_seven_k'
      "8700客流量预警 - 系统生成"
    else
      "人工添加"
    end
  end

  def unique_non_custom
    return if (custom? || door_record?)
    if self.class.send(notification_type).any?
      unless self.class.send(notification_type).pluck(:id).include?(id)
        errors.add(:base, "only one #{notification_type} allowed")
      end
    end
  end

end
