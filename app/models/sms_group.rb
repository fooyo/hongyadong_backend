# == Schema Information
#
# Table name: sms_groups
#
#  id          :bigint(8)        not null, primary key
#  name        :string(255)
#  upper_limit :integer
#  lower_limit :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  staff_ids   :string(255)
#

class SmsGroup < ApplicationRecord
  serialize :staff_ids, Array

  def staffs
    Staff.where(id: staff_ids)
  end

  def staff_names
    staffs.map{|a| a.desc}
  end

  def staff_ids=(value)
    value = value.reject {|a| a.blank?}.map {|a| a.to_i}.uniq
    write_attribute(:staff_ids, value)
  end

end
