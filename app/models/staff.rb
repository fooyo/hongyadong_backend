# == Schema Information
#
# Table name: staffs
#
#  id            :bigint(8)        not null, primary key
#  mobile_number :string(255)
#  name          :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Staff < ApplicationRecord
  def desc
    "#{name}: #{mobile_number}"
  end

  def groups
    SmsGroup.all.select{|a| a.staff_ids.include?(id)}
  end

  def group_names
    groups.pluck(:name)
  end
end
