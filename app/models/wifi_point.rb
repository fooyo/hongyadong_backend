# == Schema Information
#
# Table name: wifi_points
#
#  id                :bigint(8)        not null, primary key
#  floor_id          :bigint(8)
#  point_id          :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name              :string(255)
#  realtime_visitors :integer
#  x                 :float(24)        default(0.0)
#  y                 :float(24)        default(0.0)
#

class WifiPoint < ApplicationRecord
  belongs_to :floor, touch: true
  delegate :sub_scenic_id, to: :floor, allow_nil: true

  def number_of_connections
    realtime_visitors || 2222
  end

end
