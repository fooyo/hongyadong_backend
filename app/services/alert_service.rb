class AlertService
  def initialize
    @current_traffic = VisitorService.new.today[:number_of_realtime_visitors]
    @sms_service = SmsService.new
    @notification_service = SiteNotificationService.new
  end

  def send_alert_if_needed
    @sms_service.alert_sms_if_needed(@current_traffic)
    @notification_service.create_or_update_if_needed(@current_traffic)
  end
end