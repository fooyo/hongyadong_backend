class QcloudSmsService

  def initialize
    @app_key = Rails.application.secrets.qcloud_sms_app_key
    @app_id = Rails.application.secrets.qcloud_sms_app_id
  end

  def send_sms(tos, sms_params, template_id)
    time = Time.now.to_i
    random = Random.rand(111111..999999)
    url = group_sms_url(random)
    param = {
        params: sms_params,
        tel: tos.map {|a| {mobile: a, nationcode: "86"}},
        sign: "洪崖洞",
        time: time,
        tpl_id: template_id
    }
    param = param.merge({sig: sign_params(param, random)})
    response = RestClient.post(url, param.to_json, {'Content-Type': "application/json"})
    json = JSON.parse(response, symbolize_names: true)
    if json[:result].to_i != 0
      raise json[:errmsg]
    end
  end

  def sign_params(params, random)
    puts params
    tos = params[:tel].map {|a| a[:mobile]}.join(",")
    time = params[:time]
    str_to_sign = "appkey=#{@app_key}&random=#{random}&time=#{time}&mobile=#{tos}"
    Digest::SHA256.hexdigest(str_to_sign)
  end

  def group_sms_url(random)
    "https://yun.tim.qq.com/v5/tlssmssvr/sendmultisms2?sdkappid=#{@app_id}&random=#{random}"
  end
end