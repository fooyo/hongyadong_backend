class SiteNotificationService

  def create_or_update_if_needed(traffic)
    now = Time.now
    time_s = I18n.l(now, locale: :'zh-CN', format: :long)
    case traffic
    when 18000...20000
      traffic_reached = "eight_k"
      text = "#{time_s}景区人流监控黄色预警：各位游客，景区实时客流已经达到18000人，景区即将进入客流管控。【洪崖洞】"
    when 20000...100_000_000
      text = "#{time_s}景区人流监控红色预警：各位游客，景区实时客流已经达到20000人，景区开始客流管控，请大家按秩序排队，也可以选择错峰出行。【洪崖洞】"
      traffic_reached = "eight_seven_k"
    else
      #destroy other non custom
      SiteNotification.where.not(notification_type: ["custom","door_record"]).destroy_all
      return false
    end
    # create/update
    s = SiteNotification.where(notification_type: traffic_reached).first_or_initialize
    if s.new_record?
      s.notification_type = traffic_reached
      s.content = text
      s.save!
    else
      s.update!(content: text)
    end

    SiteNotification.where.not(notification_type: ["custom", "door_record", traffic_reached]).update_all(deleted: true)
    true

  end

end