class SmsService

  def initialize
    @normal_alert_tpl_id = 134642 # 5,6,7k visitors
    @yellow_alert_tpl_id = 134645 #8k
    @red_alert_tpl_id = 134652 #8.5k
    @red_full_alert_tpl_id = 134654 #8.7k, Full
    @qcloud_service = QcloudSmsService.new
    @sent_time_interval = 30.minutes
  end

  def alert_sms_if_needed(traffic)
    return false unless is_working_hour?
    case traffic
    when 15000...18000
      group = SmsGroup.where(lower_limit: 15000, upper_limit: 18000).first
      tpl_id = @normal_alert_tpl_id
      traffic_reached = "15000"
    when 18000...20000
      traffic_reached = "18000"
      group = SmsGroup.where(lower_limit: 18000, upper_limit: 20000).first
      tpl_id = @yellow_alert_tpl_id
    when 20000...100_000_000
      traffic_reached = "20000"
      group = SmsGroup.where(lower_limit: 20000).first
      tpl_id = @red_full_alert_tpl_id
    else
      return false
    end
    send_if_needed(group, tpl_id, traffic_reached)
  end

  def is_working_hour?
    now = Time.now
    start = now.beginning_of_day + 7.hours
    night = now.beginning_of_day + 23.hours
    now >= start && now <= night
  end

  def send_if_needed(group, tpl_id, traffic_reached)
    return false unless group
    cache_key = "group_id=#{group.id},tpl_id=#{tpl_id},traffic_reached=#{traffic_reached}"
    now = Time.now
    Rails.logger.info "Try Send Alert SMS: #{cache_key}, at #{now}"

    last_sent_at = Rails.cache.fetch(cache_key) || now.yesterday
    if now - last_sent_at < @sent_time_interval
      Rails.logger.info "Last Sent At #{last_sent_at}, abort Send, #{cache_key}"
      return false
    end

    time_s = I18n.l(now, locale: :'zh-CN', format: :long)
    sms_params = [time_s, traffic_reached]
    mobile_numbers = group.staffs.pluck(:mobile_number)
    begin
      @qcloud_service.send_sms(mobile_numbers, sms_params, tpl_id)
      Rails.cache.write(cache_key, Time.now)
      Rails.logger.info "Sent Alert SMS: #{cache_key}, at #{now}"
      return true
    rescue => e
      Rails.logger.info "Failed Send Alert SMS: #{cache_key}, error: #{e.message}, at #{now}"
      return false
    end
  end

end