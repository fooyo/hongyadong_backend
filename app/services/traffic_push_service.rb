class TrafficPushService

  def generate_door_record_notification
    time = Time.now
    span = time.beginning_of_day..time.end_of_day
    today = VisitorService.new.today
    enter_count = today[:number_of_enter_visitors]
    leave_count = today[:number_of_leave_visitors]
    content = "截止#{time.year}年#{time.month}月#{time.day}日#{time.hour}点整，今日累计入园人数#{enter_count}人，累计出园人数#{leave_count}人。"
    SiteNotification.create(notification_type: "door_record", content: content, leave_count: leave_count, enter_count: enter_count)
  end

  # records.each do |r|
  #   content = r.content
  #   puts content
  #   enter = content.split("，")[1].gsub("今日累计入园人数", "").gsub("人", "").to_i
  #   leave = content.split("，")[2].gsub("累计出园人数", "").gsub("人", "").to_i
  #   puts "enter #{enter}, leave: #{leave} "
  #   r.update_columns(enter_count: enter, leave_count: leave)
  # end
  #
  # mm = 0
  # day_count = 0
  # _records = SiteNotification.unscoped.door_record.where.not(created_at: duanwu[0].. duanwu[1]).where.not(created_at: wuyi[0]..wuyi[1]).where.not(created_at: shiyi[0]..shiyi[1])
  # while start < last
  #   s = start.beginning_of_day
  #   e = start.end_of_day
  #   m = _records.where(created_at: s..e).pluck(:enter_count).max || 0
  #   start += 1.days
  #   day_count += 1
  #   puts m
  #   mm += m
  # end
end