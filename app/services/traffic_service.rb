class TrafficService

  #人流量api
  def initialize
    @app_secret = Rails.application.secrets.wifi_app_secret
    @base_url = Rails.application.secrets.wifi_base_url
    @request_time_fmt = "%Y-%-m-%-d %H:%M:%S"
  end

  # 今日入园：[wifi_realtime+door_leave]
  # 今日接待：入园
  # 今日出园：[door]
  def today_traffic
    r = Hash.new
    wifi_result = wifi_today_traffic
    return wifi_result

    door_result = door_record_today_traffic
    wifi_result.each do |k, v|
      door_value = door_result[k]
      if k == :number_of_today_visitors
        # r[k] = [door_value, v].max
      elsif k == :number_of_leave_visitors
        r[k] = door_value
      elsif k == :number_of_enter_visitors
        # r[k] = door_value # not used
      end
    end
    r
  end


  def sync_streams
    url = "#{@base_url}/pointPosition"
    _params = {
        scenicId: 1,
        channelType: 1,
        requestTime: Time.now.strftime(@request_time_fmt)
    }
    sign = encrypt(_params)
    _params = _params.merge({sign: sign}).to_param
    response = RestClient.get "#{url}?#{_params}"
    json = JSON.parse(response.body, symbolize_names: true)
    if json.dig(:status).to_i == 0 # success
      if json[:data][:videoInfo].present?
        json[:data][:videoInfo].each do |point|
          floor = Floor.where(sub_scenic_id: point[:subScenicId]).first_or_initialize
          floor.sub_scenic_id = point[:subScenicId]
          floor.save
          hls_url = nil
          if point[:videoUrl].present?
            hls_url = point[:videoUrl].chomp.end_with?("m3u8") ? point[:videoUrl].chomp : nil
          end
          l = LiveStream.where(point_id: point[:pointId]).first_or_initialize
          l.floor_id = floor.id
          l.name = point[:pointName].gsub("-相机","").gsub("-球机","")
          l.point_id = point[:pointId]
          l.hls_url = hls_url
          l.save
        end

      end
    end
  end

  def sync_wifi_stream
    begin
      sync_streams
    end
    url = "#{@base_url}/pointBasicInfo"
    _params = {
        scenicId: 1,
        requestTime: Time.now.strftime(@request_time_fmt)
    }
    sign = encrypt(_params)
    _params = _params.merge({sign: sign}).to_param
    response = RestClient.get "#{url}?#{_params}"
    json = JSON.parse(response.body, symbolize_names: true)
    if json.dig(:status).to_i == 0 # success
      json[:data].each do |point|
        type = point[:type]

        floor = Floor.where(sub_scenic_id: point[:subScenicId]).first_or_initialize
        floor.sub_scenic_id = point[:subScenicId]
        floor.save

        if type == 1 #live_stream

          #不使用这个api同步直播信息
          # hls_url = nil
          # if point[:videoUrl].present?
          #   hls_url = point[:videoUrl].chomp.end_with?("m3u8") ? point[:videoUrl].chomp : nil
          # end
          # l = LiveStream.where(point_id: point[:pointId]).first_or_initialize
          # l.floor_id = floor.id
          # l.name = point[:pointName]
          # l.point_id = point[:pointId]
          # l.hls_url = hls_url
          # l.save

        elsif type == 2 # wifi
          l = WifiPoint.where(point_id: point[:pointId]).first_or_initialize
          l.floor_id = floor.id
          l.name = point[:pointName]
          l.point_id = point[:pointId]
          l.save
        end
      end
    end
  end

  # only level 11 has queue count
  def queue_by_level
    url = "#{@base_url}/queuingPopulation"
    _params = {
        scenicId: 1,
        subScenicId: 11,
        requestTime: Time.now.strftime(@request_time_fmt)
    }
    sign = encrypt(_params)
    _params = _params.merge({sign: sign}).to_param
    response = RestClient.get "#{url}?#{_params}"
    json = JSON.parse(response.body, symbolize_names: true)
    Rails.logger.info "queuingPolulation Result: #{json}"
    if json.dig(:status).to_i == 0 # success
      if json[:data].present?
        sub_scenic_id = json[:data][0][:subScenicId]
        count = json[:data][0][:count]
        floor = Floor.find_by(sub_scenic_id: sub_scenic_id)
        floor.update(queue_count: count)
      end
    end
  end

  def realtime_traffic_by_wifi
    sync_wifi_stream
    json = realtime_count(3)
    puts json
    Rails.logger.info "passengerFlows/realTime by wifi Result: #{json}"
    if json.dig(:status).to_i == 0 # success
      json[:data].each do |ld|
        f = WifiPoint.find_by(point_id: ld[:id].to_i)
        next unless f
        f.update(realtime_visitors: ld[:value])
      end
    end
  end

  # update each floor's realtime visitor count
  def realtime_traffic_by_level
    sync_wifi_stream
    json = realtime_count(2)
    Rails.logger.info "passengerFlows/realTime Result: #{json}"
    if json.dig(:status).to_i == 0 # success
      json[:data].each do |ld|
        f = Floor.find_by(sub_scenic_id: ld[:id].to_i)
        next unless f
        f.update(realtime_visitors: ld[:value])
      end
    end
  end

  # type 1 景区，2楼层，3 wifi
  def realtime_count(type)
    url = "#{@base_url}/passengerFlows/realTime"
    _params = {
        scenicId: 1,
        type: type,
        requestTime: Time.now.strftime(@request_time_fmt)
    }
    sign = encrypt(_params)
    _params = _params.merge({sign: sign}).to_param
    response = RestClient.get "#{url}?#{_params}"
    JSON.parse(response.body, symbolize_names: true)
  end

  def wifi_today_traffic
    url = "#{@base_url}/passengerFlows/dayCount"
    url = 'http://hongyadong-camera.1000fun.com/peopleFlowStatistic/getMonitoringStatistic'
    _params = {
        scenicId: 1,
        requestTime: Time.now.strftime(@request_time_fmt)
    }
    sign = encrypt(_params)
    _params = _params.merge({sign: sign}).to_param
    response = RestClient.get "#{url}?#{_params}"
    json = JSON.parse(response.body, symbolize_names: true)

    enter_count = 0
    leave_count = 0
    today_count = 0

    if json.dig(:status).to_i == 0 # success
      if json[:data].present?
        enter_count = json[:data][:enterScenicCount].to_i
        leave_count = json[:data][:leaveScenicCount].to_i
        today_count = json[:data][:total].to_i
      end
    end
    {
        number_of_enter_visitors: enter_count,
        number_of_leave_visitors: leave_count,
        number_of_today_visitors: today_count
    }
  end

  def door_record_today_traffic
    # today_enter + yesterday(enter-leave)
    today_range = Time.now.beginning_of_day..Time.now.end_of_day

    yesterday_range = (Time.now - 1.days).beginning_of_day..(Time.now - 1.days).end_of_day
    yesterday_dr = DoorRecord.where(reported_at: yesterday_range).first

    yesterday_stay = 0
    yesterday_stay = (yesterday_dr.enter_count - yesterday_dr.leave_count) if yesterday_dr

    today_dr = DoorRecord.where(reported_at: today_range).first

    enter_count = today_dr.try(:enter_count) || 0
    leave_count = today_dr.try(:leave_count) || 0
    today_count = enter_count + yesterday_stay
    {
        number_of_enter_visitors: enter_count,
        number_of_leave_visitors: leave_count,
        number_of_today_visitors: today_count
    }
  end


  def encrypt(_params)
    str = ""
    _params = Hash[_params.sort_by {|k, v| k.to_s}]
    _params.each do |k, v|
      str += "#{k}=#{CGI.escape(v.to_s)}&"
    end
    str += "apiSecret=#{@app_secret}"
    Digest::MD5.hexdigest(str)
  end

end