class VisitorService

  PEAK_TRAFFIC = 22000.0

  def initialize
    @traffic_service = TrafficService.new
    @cache_time = 5.seconds
    @cache_key = "visitors_today"
  end

  def today
    r = Rails.cache.fetch(@cache_key, expires_in: @cache_time) do
      traffic_hash = @traffic_service.today_traffic
      begin
        Timeout::timeout(10) do
          @traffic_service.realtime_traffic_by_level rescue nil
        end
        Timeout::timeout(10) do
          @traffic_service.queue_by_level rescue nil
        end
      rescue
        nil
      end

      visitor_offset = VisitorOffset.first_or_initialize

      # special arrangement for offset
      now = Time.now
      if now - now.beginning_of_day <= 5.minutes
        start = now.yesterday
      else
        start = now
      end

      start_drop = start.beginning_of_day + 23.hours + 50.minutes # start dop at 11:50pm
      end_drop = start.beginning_of_day + 24.hours + 5.minutes # end dop at 12:05pm
      if now >= start_drop && now <= end_drop
        traffic_hash[:number_of_enter_visitors] = visitor_offset.enter_at_eleven - visitor_offset.enter_offset
        traffic_hash[:number_of_leave_visitors] = visitor_offset.leave_at_eleven - visitor_offset.leave_offset
        traffic_hash[:number_of_today_visitors] = visitor_offset.today_at_eleven - visitor_offset.today_visitor_offset

        # increase leave count, so that realtime drop to ~zero
        step = (traffic_hash[:number_of_enter_visitors] - traffic_hash[:number_of_leave_visitors] + 50) / (15 * 60).floor
        seconds = now - start_drop
        t = (step * seconds)
        traffic_hash[:number_of_leave_visitors] = (traffic_hash[:number_of_leave_visitors] + t).floor
      else
        traffic_hash[:number_of_enter_visitors] = traffic_hash[:number_of_enter_visitors] - visitor_offset.enter_offset
        traffic_hash[:number_of_leave_visitors] = traffic_hash[:number_of_leave_visitors] - visitor_offset.leave_offset
        traffic_hash[:number_of_today_visitors] = traffic_hash[:number_of_today_visitors] - visitor_offset.today_visitor_offset
      end

      # 国庆


      # total_realtime = Floor.all.sum(:realtime_visitors) || 10
      total_realtime = traffic_hash[:number_of_enter_visitors] - traffic_hash[:number_of_leave_visitors]
      total_realtime = total_realtime < 0 ? 0 : total_realtime

      if tensorflow_queue?
        total_queue = Floor.find_by(sub_scenic_id: 11).try(:queue_count) || 0
      else
        total_queue = 0
      end

      queue_time = total_queue / 60 # seconds
      queue_time = queue_time > 210 ? queue_time / 60.0 : 0

      traffic_hash.merge(
          {
              site_capacity: site_capacity(total_realtime),
              number_of_realtime_visitors: total_realtime,
              number_of_visitors_in_queue: total_queue,
              estimated_waiting_time: queue_time,
              visitor_volume_status: visitor_volume_status(total_realtime),
              queue_stream_url: "http://183.230.181.217:40253/live/cameraid/1000001%240/substream/1.m3u8"
          }
      )
    end
    r.merge({date: Time.now.iso8601(3)})
  end

  def refresh_tensorflow_queue
    r = false
    begin
      # url = "118.24.92.119:11111/default/checkVideo"
      url = "localhost:11111/default/checkVideo"
      response = RestClient.post url, {}
      json = JSON.parse(response.body)
      Rails.logger.info "Tensorflow Result: #{json}"
      r = json["res"] != "no"
    rescue
      r = false
    end
    Rails.cache.write("tensorflow_queue?", r)
  end

  # check if tensorflow finds ppl queueing
  def tensorflow_queue?
    Rails.cache.read("tensorflow_queue?")
  end

  private

  def visitor_volume_status(count)
    if count >= 8700
      "客流拥挤"
    elsif count > 8500
      "客流缓行"
    else
      "客流畅通"
    end
  end

  def site_capacity(count)
    a = count.to_f / PEAK_TRAFFIC
    a > 0.98 ? 0.98 : a
  end

end