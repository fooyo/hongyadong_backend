class WeatherService

  def initialize
    @key = Rails.application.secrets.juhe_app_key
    @area_id = 101040100 #http://v.juhe.cn/xiangji_weather/areaList.php?key=APPKEY
    @juhe_hour_fmt = "%Y%m%d%H"
    @juhe_base_url = "http://v.juhe.cn/xiangji_weather"
    @xinzhi_weather_url = 'https://api.seniverse.com/v3/weather/now.json'
    @xinzhi_key = "k3e7dodimqszjcmn"
  end

  def current_weather
    xinzhi_current_weather
    # juhe_current_weather
  end

  def hourly_forecasts
    # juhe_weather_hourly_forecasts
  end

  def xinzhi_current_weather
    cache_key = "xinzhi_current_weather"
    cached_result = Rails.cache.read(cache_key)
    Rails.logger.info "Query cached xinzhi current weather"
    return cached_result if cached_result

    _params = {
        location: 'chongqing',
        key: @xinzhi_key
    }
    response = RestClient.get(@xinzhi_weather_url, {params: _params})
    json = JSON.parse(response.body, symbolize_names: true)
    time = Time.parse(json[:results].first[:last_update])
    weather = xinzhi_weather_for_code(json[:results].first[:now][:code])
    temp = json[:results].first[:now][:temperature].to_f
    result = {
        time: time,
        weather: weather,
        temperature: temp
    }
    Rails.cache.write(cache_key, result, expires_in: 20.minutes)
  end

  def xinzhi_weather_for_code(code)
    code = code.to_i
    case code
    when 0...4 # 0,1,2,3
      "sunny"
    when 4...9
      'cloud'
    when 9
      'overcast'
    when 10...19
      'rain'
    when 20...25
      'snow'
    else
      'overcast'
    end
  end

  private

  def juhe_weather_hourly_forecasts
    # check cache
    cache_key = "juhe_hourly_forecasts"
    cached_result = Rails.cache.read(cache_key)
    Rails.logger.info "Query cached weather forecast"
    return cached_result if cached_result

    # next 4 hours' weather forecast
    start_time = Time.now.end_of_hour + 1.seconds #next hour
    end_time = start_time + 3.hours
    start_str = start_time.strftime(@juhe_hour_fmt)
    end_str = end_time.strftime(@juhe_hour_fmt)

    # API call to juhe
    url = "#{@juhe_base_url}/weather_byHour_areaid.php?areaid=#{@area_id}&startTime=#{start_str}&endTime=#{end_str}&key=#{@key}"
    response = RestClient.get(url)
    json = JSON.parse(response.body, symbolize_names: true)
    Rails.logger.info "Query Juhe Current Weather API"

    # parse response

    if json.dig(:reason) == "success"
      offset = 0
      result = json[:result][:series].map do |v|
        weather_code = v[:cw].to_i
        weather = juhe_weather_code_to_weather(weather_code)
        temperature = v[:tmp]
        time = (start_time + offset.hours).iso8601(3)
        offset += 1
        {
            time: time,
            weather: weather,
            temperature: temperature
        }
      end
      Rails.cache.write(cache_key, result, expires_in: 20.minutes)
      result
    end
  end

  def juhe_current_weather

    # check cache
    cache_key = "juhe_current_weather"
    cached_result = Rails.cache.read(cache_key)
    Rails.logger.info "Query cached current weather"
    return cached_result if cached_result

    url = "#{@juhe_base_url}/real_time_weather.php?areaid=#{@area_id}&key=#{@key}"
    response = RestClient.get(url)
    json = JSON.parse(response.body, symbolize_names: true)
    Rails.logger.info "Query Juhe Current Weather API"

    if json.dig(:reason) == "success"
      weather_code = json[:result][:data][:cw].to_i
      weather = juhe_weather_code_to_weather(weather_code)
      result = {
          weather: weather,
          temperature: json[:result][:data][:tmp],
          time: Time.now.iso8601(3)
      }
      Rails.cache.write(cache_key, result, expires_in: 20.minutes)
      result
    end
  end

  def juhe_weather_code_to_weather(code)
    #refer to 724天气标识.csv
    case code
    when 0
      w = "sunny"
    when 1
      w = "cloud"
    when 2, 18
      w = "overcast"
    when 3..12, 19..25, 301
      w = "rain"
    when 13..17, 26..28
      w = "snow"
    else
      w = "overcast"
    end
    w
  end


end