json.site_notifications @site_notifications do |n|
  json.partial! 'v1/site_notifications/site_notification', site_notification: n
end