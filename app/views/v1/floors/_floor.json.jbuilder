json.extract! floor, :id, :floor_image, :realtime_visitors, :queue_count,
              :name, :floor, :visitor_volume_status

json.wifi_points floor.wifi_points do |l|
  json.partial! 'v1/wifi_points/wifi_point', wifi_point: l
end

json.live_streams floor.live_streams do |l|
  json.partial! 'v1/live_streams/live_stream', live_stream: l
end
