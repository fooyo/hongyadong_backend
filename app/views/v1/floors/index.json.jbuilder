json.cache! @floors do
  json.floors @floors do |f|
    json.partial! 'floor', floor: f
  end
end