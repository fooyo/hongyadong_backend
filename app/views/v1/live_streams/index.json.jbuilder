json.cache! @live_streams do
  json.live_streams @live_streams do |l|
    json.partial! 'live_stream', live_stream: l
  end
end