json.site_notifications @site_notifications do |n|
  json.partial! 'site_notification', site_notification: n
end