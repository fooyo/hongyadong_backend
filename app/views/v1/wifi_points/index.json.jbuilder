json.cache! @wifi_points do
  json.wifi_points @wifi_points do |w|
    json.partial! 'wifi_point', wifi_point: w
  end
end