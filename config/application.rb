require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module HongyadongBackend
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    I18n.available_locales = [:en, :'zh-CN']
    config.i18n.default_locale = :'zh-CN'

    Raven.configure do |config|
      config.dsn = 'https://d867b08f31504ef8a5da12bc37ecf6c7:e1c1095be8304e3ca178312bea494069@sentry.io/1223541'
    end

    config.load_defaults 5.2

    config.time_zone = 'Beijing'

    allowed_origins = %w(
                        hongyadong.fooyo.sg hyd.staging.fooyo.sg
                       localhost:8080 sentosa_dashboard.staging.fooyo.sg
                        hyd.fooyotravel.com hongyadong.fooyotravel.com
                      )

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins allowed_origins
        resource '*', headers: :any, methods: [:get, :post, :options], credentials: true
      end
    end
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end