Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #
  namespace :v1 do

    resources :soap, only: [] do
      collection do
        post :sms
        get :sms
      end
    end

    resources :pageviews
    resources :live_streams, only: [:index]
    resources :site_notifications, only: [:index]
    resources :wifi_points, only: [:index]
    resources :door_records, only: [] do
      collection do
        post :report
        get :site_notifications
      end
    end

    resources :floors, only: [:index]

    resources :weather, only: [] do
      collection do
        get :current
        get :current_and_forecasts
      end
    end
    resources :visitors, only: [] do
      collection do
        get :today
      end
    end

  end
end
