# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
#
every 1.minutes do
  rake 'sync_traffic'
end

every 1.hours do
  rake 'traffic_push'
end
every 3.minutes do
  rake 'traffic_alert'
end
every 3.minutes do
  rake 'traffic_alert'
end

# every 15 minutes, start at minute 5
every '5,20,35,50 * * * *' do
  rake 'sync_tensorflow'
end

every 1.day, at: "00:05am" do
  rake 'visitor_offset:clear_offset'
end

every 1.day, at: "11:50pm" do
  rake 'visitor_offset:sync_eleven'
end

every 1.day, at: "08:50am" do
  rake 'visitor_offset:sync_morning'
end
