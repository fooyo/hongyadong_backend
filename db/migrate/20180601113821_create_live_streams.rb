class CreateLiveStreams < ActiveRecord::Migration[5.2]
  def change
    create_table :live_streams,options: 'ROW_FORMAT=DYNAMIC' do |t|
      t.string :hls_url
      t.string :name
      t.timestamps
    end
  end
end
