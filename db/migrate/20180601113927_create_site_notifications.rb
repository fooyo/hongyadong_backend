class CreateSiteNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :site_notifications,options: 'ROW_FORMAT=DYNAMIC' do |t|
      t.string :content
      t.timestamps
    end
  end
end
