class CreateDoorRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :door_records,options: 'ROW_FORMAT=DYNAMIC' do |t|
      t.integer :record_type, index: true
      t.timestamps
    end
  end
end
