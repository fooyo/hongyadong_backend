class RefactorDoorRecords < ActiveRecord::Migration[5.2]

  def change
    remove_column :door_records, :record_type
    add_column :door_records, :reported_at, :datetime
    add_column :door_records, :enter_count, :integer
    add_column :door_records, :leave_count, :integer
  end

end
