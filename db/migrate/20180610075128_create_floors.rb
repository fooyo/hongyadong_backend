class CreateFloors < ActiveRecord::Migration[5.2]
  def change
    create_table :floors,options: 'ROW_FORMAT=DYNAMIC' do |t|
      t.string :name
      t.integer :sub_scenic_id # from wifi
      t.integer :queue_count, default: 0
      t.timestamps
    end
    add_column :live_streams, :floor_id, :integer, index: true
    add_column :live_streams, :point_id, :integer, index: true
  end
end
