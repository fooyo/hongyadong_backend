class CreateWifiPoints < ActiveRecord::Migration[5.2]
  def change
    create_table :wifi_points,options: 'ROW_FORMAT=DYNAMIC' do |t|
      t.belongs_to :floor, index: true
      t.integer :point_id, index: true
      t.timestamps
    end
  end
end
