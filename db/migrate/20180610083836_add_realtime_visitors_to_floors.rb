class AddRealtimeVisitorsToFloors < ActiveRecord::Migration[5.2]
  def change
    add_column :floors, :realtime_visitors, :integer
  end
end
