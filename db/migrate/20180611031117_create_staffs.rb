class CreateStaffs < ActiveRecord::Migration[5.2]
  def change
    create_table :staffs,options: 'ROW_FORMAT=DYNAMIC' do |t|
      t.string :mobile_number
      t.string :name
      t.timestamps
    end
  end
end
