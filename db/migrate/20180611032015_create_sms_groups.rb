class CreateSmsGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :sms_groups,options: 'ROW_FORMAT=DYNAMIC' do |t|
      t.string :name
      t.integer :upper_limit
      t.integer :lower_limit
      t.timestamps
    end
  end
end
