class AddNameToWifiPoints < ActiveRecord::Migration[5.2]
  def change
    add_column :wifi_points, :name, :string
  end
end
