class AddRealtimeVisitorsToWifiPoints < ActiveRecord::Migration[5.2]
  def change
    add_column :wifi_points, :realtime_visitors, :integer
  end
end
