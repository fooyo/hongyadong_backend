class AddNotificationTypeToSiteNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :site_notifications, :notification_type, :integer, default: 0
  end
end
