class AddMaxVisitorsToFloors < ActiveRecord::Migration[5.2]
  def change
    add_column :floors, :max_visitors, :integer
  end
end
