class AddStaffIdsToSmsGroup < ActiveRecord::Migration[5.2]
  def change
    add_column :sms_groups, :staff_ids, :string
  end
end
