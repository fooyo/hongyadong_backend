class AddXAndYToWifiPoints < ActiveRecord::Migration[5.2]
  def change
    add_column :wifi_points, :x, :float, default: 0.0
    add_column :wifi_points, :y, :float, default: 0.0
  end
end
