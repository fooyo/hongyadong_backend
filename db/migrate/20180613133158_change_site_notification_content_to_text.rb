class ChangeSiteNotificationContentToText < ActiveRecord::Migration[5.2]
  def change
    change_column :site_notifications, :content, :text
  end
end
