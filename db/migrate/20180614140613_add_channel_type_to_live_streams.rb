class AddChannelTypeToLiveStreams < ActiveRecord::Migration[5.2]
  def change
    add_column :live_streams, :channel_type, :integer, index: true
  end
end
