class CreatePageviews < ActiveRecord::Migration[5.2]
  def change
    create_table :pageviews do |t|
      t.integer :count, default: 0
      t.timestamps
    end
  end
end
