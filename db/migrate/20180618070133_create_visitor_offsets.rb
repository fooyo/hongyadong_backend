class CreateVisitorOffsets < ActiveRecord::Migration[5.2]
  def change
    create_table :visitor_offsets do |t|
      t.integer :enter_offset
      t.integer :leave_offset
      t.integer :today_visitor_offset
      t.integer :enter_at_eleven
      t.integer :leave_at_eleven
      t.timestamps
    end
  end
end
