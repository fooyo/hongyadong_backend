class AddTodayAtElevenToVisitorOffset < ActiveRecord::Migration[5.2]
  def change
    add_column :visitor_offsets, :today_at_eleven, :integer, default: 0
    change_column :visitor_offsets,:enter_offset,:integer, default: 0
    change_column :visitor_offsets,:leave_offset,:integer, default: 0
    change_column :visitor_offsets,:enter_at_eleven,:integer, default: 0
    change_column :visitor_offsets,:leave_at_eleven,:integer, default: 0
    change_column :visitor_offsets,:today_visitor_offset,:integer, default: 0
  end
end
