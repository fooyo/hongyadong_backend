class AddUseTensorflowToVisitorOffset < ActiveRecord::Migration[5.2]
  def change
    add_column :visitor_offsets, :use_tensorflow, :boolean, default: true
  end
end
