class AddDeletedToSiteNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :site_notifications, :deleted, :boolean, default: false
  end
end
