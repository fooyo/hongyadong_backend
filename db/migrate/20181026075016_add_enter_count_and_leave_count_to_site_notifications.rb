class AddEnterCountAndLeaveCountToSiteNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :site_notifications, :enter_count, :integer
    add_column :site_notifications, :leave_count, :integer
  end
end
