# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#

# [
#     'http://live.hkstv.hk.lxdns.com/live/hks/playlist.m3u8',
#     'http://ivi.bupt.edu.cn/hls/cctv1hd.m3u8',
#     'http://ivi.bupt.edu.cn/hls/cctv3hd.m3u8',
#     'http://ivi.bupt.edu.cn/hls/cctv5hd.m3u8',
#     'http://ivi.bupt.edu.cn/hls/cctv5phd.m3u8',
#     'http://ivi.bupt.edu.cn/hls/cctv6hd.m3u8'
# ].each_with_index do |hls, idx|
#   LiveStream.create(name: "景区#{idx+1}楼", hls_url: hls)
# end
#
#
# AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?
#

x = %w(70 134 174 298 482 490 646 658 802 838 866 994 1078 1222 1346 1338 314 450 566
614 722 878 842 910 1042 1042 1102 1174 1174 1286 1382 1382 178 214 326
358 534 610 682 802 830 910 998 1110 1230 34 94 242 322 490 510 670
762 822 990 34 122 218 742 170 294 558 790 858 282 282 666 998 1094)

y = %w( 378 302 422 486 410 534 410 534 270 534 410 534 410 534 410 294 502 514 602 514
602 602 350 514 602 442 514 442 602 514 442 602 378 482 482 378 482 378 482 530 378
474 542 442 542 510 438 462 558 458 558 554 450 554 482 410 342 362 382
342 426 426 426 506 458 366 522 514 450 )
points = %w(
157 158 159 160 161 162 163 164 165 166 167 168 169 170
171 172 112 113 114 115 116 117 118 119 120 121 122 123
124 125 126 127 128 129 130 131 132 133 134 135 136 137 138
139 140 141 142 143 144 145 146 147 148 149 150 151
152 153 154 0 0 0 0 0 155 0
0 0 156 )
points.each_with_index do |value, idx|
  w = WifiPoint.find_by(point_id: value.to_i)
  if w
    w.update(x: x[idx].to_f, y: y[idx].to_f)
  end

end

limits = %w(5000 6000 7000 8000 8500 8700)

limits.each_with_index do |v, i|
  name = "#{i}客流量短信预警组"
  next if SmsGroup.find_by_name(name)
  next_limit = i == limits.count - 1 ? 100_000 : limits[i + 1].to_i
  SmsGroup.create(name: name, upper_limit: next_limit, lower_limit: v.to_i)
end

