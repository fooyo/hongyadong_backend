desc "Sync Wifi realtime user data"
task sync_traffic: :environment do
  Rails.logger.info "Start Sync Wifi Realtime"
  s = TrafficService.new
  s.realtime_traffic_by_wifi
end

desc "Sync Wifi realtime user data"
task sync_tensorflow: :environment do
  VisitorService.new.refresh_tensorflow_queue
end
