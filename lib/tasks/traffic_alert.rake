task traffic_alert: :environment do
  Rails.logger.info "Start Traffic Alert Checking at #{Time.now}"
  AlertService.new.send_alert_if_needed
end
