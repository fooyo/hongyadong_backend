namespace :visitor_offset do
  desc "Get offsets at 8:50am"
  task sync_morning: :environment do
    today_traffic = TrafficService.new.today_traffic
    v = VisitorOffset.first_or_initialize
    v.enter_offset = today_traffic[:number_of_enter_visitors]
    v.leave_offset = today_traffic[:number_of_leave_visitors]
    v.today_visitor_offset = today_traffic[:number_of_today_visitors]
    v.save
  end

  desc "Get enter count at 11:50pm"
  task sync_eleven: :environment do
    today_traffic = TrafficService.new.today_traffic
    v = VisitorOffset.first_or_initialize
    v.enter_at_eleven = today_traffic[:number_of_enter_visitors]
    v.leave_at_eleven = today_traffic[:number_of_leave_visitors]
    v.today_at_eleven = today_traffic[:number_of_today_visitors]
    v.save
  end

  desc "clear offset at 00:05"
  task clear_offset: :environment do
    v = VisitorOffset.first_or_initialize
    v.enter_offset = 0
    v.leave_offset = 0
    v.today_visitor_offset = 0
    v.enter_at_eleven = 0
    v.leave_at_eleven = 0
    v.today_at_eleven = 0
    v.save
  end

end

