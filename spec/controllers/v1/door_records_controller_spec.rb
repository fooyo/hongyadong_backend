require 'rails_helper'

RSpec.describe V1::DoorRecordsController, type: :controller do
  render_views

  describe "get door records notifications" do
    it "should return notifications" do
      post :report, params: {
          door_record: {
              enter_count: 1,
              leave_count: 30,
              reported_at: Time.now
          }
      }
      TrafficPushService.new.generate_door_record_notification
      get :site_notifications
      puts pretty_json
      expect(response).to have_http_status(200)
    end
  end

  describe "report door record" do
    it "should recrod an enter record" do
      post :report, params: {
          door_record: {
              enter_count: 1,
              leave_count: 30,
              reported_at: "2018-06-01T20:07:13.000+08:00"
          }
      }
      post :report, params: {
          door_record: {
              enter_count: 1,
              leave_count: 30,
              reported_at: "2018-06-01T22:07:13.000+08:00"
          }
      }
      expect(response).to have_http_status(204)
    end
  end
end
