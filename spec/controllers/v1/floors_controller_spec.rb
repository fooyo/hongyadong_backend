require 'rails_helper'

RSpec.describe V1::FloorsController, type: :controller do

  render_views

  describe "get index" do
    it "should return list of floors" do
      f = Floor.create
      f.wifi_points.create
      f.live_streams.create
      get :index
      puts pretty_json
      expect(response).to have_http_status(200)
    end
  end
end
