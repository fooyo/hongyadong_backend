require 'rails_helper'

RSpec.describe V1::LiveStreamsController, type: :controller do
  render_views

  describe "GET lives_streams" do
    it "should return list of live streams" do
      LiveStream.create(hls_url: "1", name: "name1")
      LiveStream.create(hls_url: "2", name: "name2")
      get :index
      puts pretty_json
      expect(response).to have_http_status(200)
    end
  end

end
