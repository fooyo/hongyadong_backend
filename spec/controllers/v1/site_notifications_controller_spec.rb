require 'rails_helper'

RSpec.describe V1::SiteNotificationsController, type: :controller do
  render_views

  describe "GET site_notifications" do
    it "should return list of site notification" do
      SiteNotification.create(content: "请注意倒车请注意倒车")
      SiteNotification.create(content: "请勿注意前进")
      get :index
      puts pretty_json
      expect(response).to have_http_status(200)
    end
  end

end
