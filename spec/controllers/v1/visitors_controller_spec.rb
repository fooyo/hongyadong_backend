require 'rails_helper'

RSpec.describe V1::VisitorsController, type: :controller do
  describe "GET today" do
    it "should return visitor stats of today" do
      get :today
      puts pretty_json
      expect(response).to have_http_status(200)
    end
  end
end
