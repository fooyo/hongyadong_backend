require 'rails_helper'

RSpec.describe V1::WeatherController, type: :controller do
  render_views

  describe "GET current and forecast" do
    it "should return current & forecast weather" do
      Rails.cache.clear
      get :current_and_forecasts
      puts pretty_json
      expect(response).to have_http_status(200)
    end
  end
end
