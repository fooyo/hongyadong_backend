require 'rails_helper'

RSpec.describe V1::WifiPointsController, type: :controller do
  render_views

  describe "GET wifi_points" do
    it "should return list of wifi points" do
      Floor.create.wifi_points.create
      get :index
      puts pretty_json
      expect(response).to have_http_status(200)
    end
  end
end
