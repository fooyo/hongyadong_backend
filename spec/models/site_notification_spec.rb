# == Schema Information
#
# Table name: site_notifications
#
#  id                :bigint(8)        not null, primary key
#  content           :text(65535)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  notification_type :integer          default("custom")
#  deleted           :boolean          default(FALSE)
#  enter_count       :integer
#  leave_count       :integer
#

require 'rails_helper'

RSpec.describe SiteNotification, type: :model do
  describe "Test Uniqe non custom type validator" do
    it "should be true" do
      SiteNotification.create
      s = SiteNotification.create(notification_type: 'eight_k')
      s.update!(notification_type: 'eight_k')
      expect(s.errors.to_a.count).to be(0)
    end

    it "should be false for double 5k" do
      SiteNotification.create(notification_type: 'eight_k')
      s = SiteNotification.create(notification_type: 'eight_k')
      expect(s.errors.to_a.count).to be(1)
    end
  end

end
