# == Schema Information
#
# Table name: visitor_offsets
#
#  id                   :bigint(8)        not null, primary key
#  enter_offset         :integer          default(0)
#  leave_offset         :integer          default(0)
#  today_visitor_offset :integer          default(0)
#  enter_at_eleven      :integer          default(0)
#  leave_at_eleven      :integer          default(0)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  today_at_eleven      :integer          default(0)
#  use_tensorflow       :boolean          default(TRUE)
#

require 'rails_helper'

RSpec.describe VisitorOffset, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
